import java.util.Scanner;

public class RangedWeapon extends Weapon{
	//DV TAble by weapon type
	static final int[] PISTOL_DV = {13,15,20,25,30,30,99,99};
	static final int[] SMG_DV = {15,13,15,20,25,25,30,99};
	static final int[] SHOTGUN_DV = {13,15,20,25,30,35,99,99};
	static final int[] AR_DV = {17,16,15,13,15,20,25,30};
	static final int[] SNIPER_DV = {30,25,25,20,15,16,17,20};
	static final int[] BOW_DV = {15,13,15,17,20,22,99,99};
	static final int[] GRENADE_LAUNCHER_DV = {16,15,15,17,20,22,25,99};
	static final int[] RPG_DV = {17,16,15,15,20,20,25,30};
	
	private int clipSize;
	private int clipAmmo;

	
	
	public RangedWeapon(int weaponID, String name, int damageDiceNumber, int damageDiceValue, int rateOfFire,
			int clipSize, int clipAmmo, String combatStat) {
		super(false,weaponID, name, damageDiceNumber, damageDiceValue, rateOfFire,combatStat);
		this.clipSize = clipSize;
		this.clipAmmo = clipAmmo;
	}

	public void attackWithWeapon(Fighter enemy) {
		this.setClipAmmo(getClipAmmo() - 1); 
		if(getClipAmmo() >= 0) {
			int damage = this.getDamageRoll();
			enemy.takeDamageBody(damage, false);
		} else {
			System.out.println("No bullet to shoot");
		}
		
	}

	public int getClipAmmo() {
		return clipAmmo;
	}

	public void setClipAmmo(int clipAmmo) {
		if(clipAmmo >= 0) {
			this.clipAmmo = clipAmmo;
		} else {
			this.clipAmmo = -1;
			System.out.println("No bullet to shoot");
		}
		
	}

	public int calculateDV() {
		int DV = 99;
		//prompt for range
		Scanner sc = new Scanner(System.in);
		System.out.println("What is the range of the enemy");
		int range = sc.nextInt();
		sc.close();
		//Ternary operator, give the correct index based on the range, else give -1
		int rangeIndex = 
				(0<= range &&  6 >= range) ? 0 :
			    (7 <= range &&  12 >= range) ? 1 :
			    (13 <= range &&  25 >= range) ? 2 :
		    	(26 <= range &&  50 >= range) ? 3 :
	    		(51 <= range &&  100 >= range) ? 4 :
    			(101 <= range &&  200 >= range) ? 5 :
				(201 <= range &&  400 >= range) ? 6 :
				(401 <= range &&  800 >= range) ? 7 : -1;
		
		
		if ("Medium Pistol".equals(this.getName()) || "Heavy Pistol".equals(this.getName()) || "Very Heavy Pistol".equals(this.getName())) {
		    DV = PISTOL_DV[rangeIndex];
		} else if ("SMG".equals(this.getName()) || "Heavy SMG".equals(this.getName())) {
		    DV = SMG_DV[rangeIndex];
		} else if ("Shotgun".equals(this.getName())) {
		    DV = SHOTGUN_DV[rangeIndex];
		} else if ("Assault Rifle".equals(this.getName())) {
		    DV = AR_DV[rangeIndex];
		} else if ("Sniper Rifle".equals(this.getName())) {
		    DV = SNIPER_DV[rangeIndex];
		} else if ("Bows and Crossbows".equals(this.getName())) {
		    DV = BOW_DV[rangeIndex];
		} else if ("Grenade Launchers".equals(this.getName())) {
		    DV = GRENADE_LAUNCHER_DV[rangeIndex];
		} else if ("Rocket Launchers".equals(this.getName())) {
		    DV = RPG_DV[rangeIndex];
		}

		return DV;
	}

	public void reload() {
		clipAmmo = clipSize;
	}
}
