

public class Encounter {
	private Fighter[] fighters;
	private int[] turnOrder;
	
	public Encounter(Fighter[] fighters) {
		this.fighters = fighters;
	}
	//Main combat simulation function
	public void start() {
		this.calculateTurnOrder();
		this.combatTurn(this.fighters[turnOrder[0]], this.fighters[turnOrder[1]]);
		
		
		/*Kill the character
		if (defender.gethP() <= 0) {
			defender = null;
			
		}*/
	}
	
	public void addFighter(Fighter f) {
		for(int i = 0; i< this.fighters.length;i++) {
			if(fighters[i] == null) {
				fighters[i] = f;
				break;
			}
		}
	}
	
	//Overloaded function to take either name or the object
	public void removeFighter(Fighter f) {
		removeFighter(f.getName());
	}
	public void removeFighter(String fName) {
		for (int i = 0; i < this.fighters.length;i++) {
			if(fName.equals(fighters[i].getName())) {
				fighters[i] = null;
			}
		}
	}
	
	public void calculateTurnOrder() {
		int[][] initRolls =new int[fighters.length][2];
		//For every fighter, roll a initiative check and store it in turn order
		for(int i = 0; i < fighters.length && fighters[i] != null;i++) {
			int initRoll = fighters[i].rollInitiative();
			//Store fighter's index
			initRolls[i][0] = i;
			initRolls[i][1] = initRoll;
		}
		//Sort in order of roll
		main.sortbyColumn(initRolls, 1);
		this.turnOrder = new int[initRolls.length];
		
		for(int i =0; i < initRolls.length && initRolls[i] != null; i++) {
			//Save the fighter's indexes in order of rolls
			this.turnOrder[i] = initRolls[i][0];
		}
		
		
	}
	public void combatTurn(Fighter attacker, Fighter defender) {
		//While RoF isnt 0, check for a hit
		//TODO add the ability to not use all RoF
		int rof = attacker.getWeapon().getRateOfFire();
		for(int i = rof; i != 0 ; i--) {
			//if it hits
			if(attacker.tryToHit(defender)) {
				attacker.attack(defender);
			}
			//Destroy the defender
			if (defender.gethP() <= 0) {
				defender = null;
				
			}
		}

	}
}
