
public class Armor {
	private int armorID;
	private String name;
	private int SP;
	
	public Armor(int armorID, String name, int sP) {
		this.armorID = armorID;
		this.name = name;
		this.SP = sP;
	}
	
	public int getArmorID() {
		return armorID;
	}

	public String getName() {
		return name;
	}

	public int getSP() {
		return this.SP;
	}
	
	public void setSP(int delta) {
		
		this.SP = this.SP + delta;
		
		if(this.SP < 0 ) {
			this.SP = 0;
		}
		
	}
}
