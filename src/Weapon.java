
abstract class Weapon {
	private boolean isMelee;
	private String name;
	private int weaponID;

	private String combatStat;

	private int damageDiceNumber;
	private int damageDiceValue;
	private int rateOfFire;
	
	
	public Weapon(boolean isMelee,int weaponID, String name, int damageDiceNumber, int damageDiceValue, int rateOfFire,String combatStat) {
		this.isMelee = isMelee;
		this.weaponID = weaponID;
		this.name = name;
		this.damageDiceNumber = damageDiceNumber;
		this.damageDiceValue = damageDiceValue;
		this.rateOfFire = rateOfFire;
		this.combatStat = combatStat;
	}

	public int getWeaponID() {
		return weaponID;
	}

	public String getName() {
		return name;
	}

	public int getRateOfFire() {
		return rateOfFire;
	}

	public boolean isMelee() {
		return isMelee;
	}
	
	public String getCombatStat() {
		return combatStat;
	}

	public int getDamageRoll() {
		int damageSum = 0;
		for(int i =0; i < damageDiceNumber;i++) {
			damageSum += main.rollDice(damageDiceValue);
		}
		return damageSum;
	}
	
	
	 public abstract void attackWithWeapon(Fighter Enemy);




	
}
