
public class Character {


	//static stats
	private String name;
	private int intelligence;
	private int reflex;
	private int dexterity;
	private int tech;
	private int cool;
	private int will;
	private int move;
	private int body;
	private int empathy;
	private int handGun;
	private int shoulderArms;
	private int archery;
	private int heavyWeapon;
	private int brawling;
	private int meleeWeapon;
	private int evasion;
	
	//Variable stat
	protected int hP;
	private Armor headArmor;
	private Armor chestArmor;
	private Weapon weapon;
	
	public String getName() {
		return name;
	}
	public int getIntelligence() {
		return intelligence;
	}
	public int getReflex() {
		return reflex;
	}
	public int getDexterity() {
		return dexterity;
	}
	public int getTech() {
		return tech;
	}
	public int getCool() {
		return cool;
	}
	public int getWill() {
		return will;
	}
	public int getMove() {
		return move;
	}
	public int getBody() {
		return body;
	}
	public int getEmpathy() {
		return empathy;
	}
	public int gethP() {
		return hP;
	}
	public Weapon getWeapon() {
		return weapon;
	}
	public int getEvasion() {
		return evasion;
	}

	public int getHandGun() {
		return handGun;
	}
	public int getShoulderArms() {
		return shoulderArms;
	}
	public int getArchery() {
		return archery;
	}
	public int getHeavyWeapon() {
		return heavyWeapon;
	}
	public int getBrawling() {
		return brawling;
	}
	public int getMeleeWeapon() {
		return meleeWeapon;
	}
	public Armor getHeadArmor() {
		return headArmor;
	}
	public Armor getChestArmor() {
		return chestArmor;
	}
	public int getRelevantStat(String relStat) {
		int finalStat = 0;
		if (relStat.equals("Handgun")) {
		    finalStat = this.handGun;
		} else if (relStat.equals("Shoulder Arms")) {
		    finalStat = this.shoulderArms;
		} else if (relStat.equals("Archery")) {
		    finalStat = this.archery;
		} else if (relStat.equals("Heavy Weapon")) {
		    finalStat = this.heavyWeapon;
		} else if (relStat.equals("Melee Weapon")) {
		    finalStat = this.meleeWeapon;
		} else if (relStat.equals("Brawling")) {
		    finalStat = this.brawling;
		}
		
		return finalStat;
	}
	
	public Character(String name, int intelligence, int reflex, int dexterity, int tech, int cool, int will, int move,
			int body, int empathy, int handGun, int shoulderArms, int archery, int heavyWeapon, int brawling,
			int meleeWeapon, int evasion, int hP, Armor headArmor, Armor chestArmor, Weapon weapon) {
		this.name = name;
		this.intelligence = intelligence;
		this.reflex = reflex;
		this.dexterity = dexterity;
		this.tech = tech;
		this.cool = cool;
		this.will = will;
		this.move = move;
		this.body = body;
		this.empathy = empathy;
		this.handGun = handGun;
		this.shoulderArms = shoulderArms;
		this.archery = archery;
		this.heavyWeapon = heavyWeapon;
		this.brawling = brawling;
		this.meleeWeapon = meleeWeapon;
		this.evasion = evasion;
		this.hP = hP;
		this.headArmor = headArmor;
		this.chestArmor = chestArmor;
		this.weapon = weapon;
	}

	
}

