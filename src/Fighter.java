
public class Fighter extends Character{
	
	public Fighter(String name, int intelligence, int reflex, int dexterity, int tech, int cool, int will, int move,
			int body, int empathy, int handGun, int shoulderArms, int archery, int heavyWeapon, int brawling,
			int meleeWeapon, int evasion, int hP, Armor headArmor, Armor chestArmor, Weapon weapon) {
		
		super(name,  intelligence,  reflex,  dexterity,  tech,  cool,  will,  move,
			 body,  empathy,  handGun,  shoulderArms,  archery,  heavyWeapon,  brawling,
			 meleeWeapon,  evasion,  hP,  headArmor,  chestArmor,  weapon);
	}
	
	public Fighter(Character c) {
		super(c.getName(),  c.getIntelligence(),  c.getReflex(),  c.getDexterity(),  c.getTech(),  c.getCool(),  c.getWill(),  c.getMove(),
				c.getBody(),  c.getEmpathy(),  c.getHandGun(),  c.getShoulderArms(),  c.getArchery(),  c.getHeavyWeapon(),  c.getBrawling(),
				c.getMeleeWeapon(),  c.getEvasion(),  c.hP,  c.getHeadArmor(),  c.getChestArmor(),  c.getWeapon());
		
	}
	public void setHealth(int newHealth) {
		this.hP = newHealth;
	}
	


	public void takeDamageBody(int damage,boolean isMelee) {
		int damageTaken = 0;
		int SP;
		if(isMelee) {
			SP = (int) Math.floor(this.getChestArmor().getSP()/2);
		} else {
			SP = this.getChestArmor().getSP();
		}
		
		if(SP >= damage) {
			damageTaken = 0;
		}
		else{
			damageTaken = damage - SP;
			this.getChestArmor().setSP(-1);
		}
		
		this.setHealth( this.gethP() - damageTaken);
	}
	
	public void takeDamageHead(int damage,boolean isMelee) {
		int damageTaken = 0;
		int SP;
		if(isMelee) {
			SP = (int) Math.floor(this.getHeadArmor().getSP()/2);
		} else {
			SP = this.getHeadArmor().getSP();
		}
		
		if(SP >= damage) {
			damageTaken = 0;
		}
		else{
			damageTaken = (damage - SP)* 2;
			this.getHeadArmor().setSP(-1);
		}
		
		this.setHealth( this.gethP() - damageTaken);
	}

	public void attack(Fighter enemy) {
		this.getWeapon().attackWithWeapon(enemy);
	}

	
	public boolean tryToHit(Character enemy) {
		boolean hitSuccess;
		int checkDiceSide = 10;
		int relStat = this.getRelevantStat(this.getWeapon().getCombatStat());
		int attackerValue;
		int defenderValue;
		if(this.getWeapon().isMelee()) {
			//isMelee
			attackerValue = this.getDexterity() + relStat + main.rollDice(checkDiceSide);
			defenderValue = enemy.getDexterity() + enemy.getEvasion() + main.rollDice(checkDiceSide);		
		} else {
			//isRanged	
			attackerValue = this.getReflex() + relStat + main.rollDice(checkDiceSide);
			//NO WAY THIS DOESNT BLOW UP
			RangedWeapon rangedWeap = (RangedWeapon) getWeapon();
			defenderValue = rangedWeap.calculateDV();
		}
		
		if(attackerValue > defenderValue) {
			System.out.println("It's a hit");
			hitSuccess = true;
		} else {
			System.out.println("It's a miss");
			hitSuccess = false;
		}
		
		
		return hitSuccess;
	}

	public int rollInitiative(){
		
		return this.getReflex() + main.rollDice(10);
	}
}
