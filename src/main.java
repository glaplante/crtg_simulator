import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;

public class main {
	static final boolean MANUAL_DICES = false;
	
	public static int rollDice (int side) {
		int rollResult;
		if(MANUAL_DICES) {
			Scanner sc = new Scanner(System.in);
			System.out.println("What is the roll of this dice?");
			rollResult = sc.nextInt();
			sc.close();
		} else {
		 rollResult = (int)(Math.random() *  (side+1));
		}
		
		 return rollResult;
	}	
    //Thief is good
	public static void sortbyColumn(int arr[][], final int col) {
       
		Arrays.sort(arr, new Comparator<int[]>() {
			@Override
		    public int compare(int[] a, int[] b) {
				return Integer.valueOf(b[col]).compareTo(a[col]);
		    }
		}); // increasing order
    }
    
	public static int getIndexOfWeaponByName(Weapon[] weapons,String name) {
		int index = -1;
		for(int i = 0; i < weapons.length; i++) {
			if(weapons[i].getName().equals(name)) {
				index = weapons[i].getWeaponID();
			}
			
		}
		return index;
	}
	public static int getIndexOfArmorByName(Armor[] armors,String name) {
		int index = -1;
		for(int i = 0; i < armors.length; i++) {
			if(armors[i].getName().equals(name)) {
				index = armors[i].getArmorID();
			}
			
		}
		return index;
	}
	
	public static void main(String[] args) {
		//Load everything
		Armor[] armors = loadArmor("gameData\\armor.csv");
		MeleeWeapon[] meleeWeapons = loadMeleeWeapon("gameData\\\\melee.csv");
		RangedWeapon[] rangedWeapons = loadRangedWeapon("gameData\\\\ranged.csv");
		Character[] mookTemplate = loadCharacter("gameData\\\\mook.csv",armors,meleeWeapons,rangedWeapons);
		Character[] playerChars = loadCharacter("gameData\\\\players.csv",armors,meleeWeapons,rangedWeapons);
		Fighter[] players = convertToFighter(playerChars);
		//Fighter[] players = loadFighter("players.csv",armors,meleeWeapons,rangedWeapons);
		
		
		//Implement main loop, probably 2 arrays, one for players and one for enemies
		
		
		Fighter mook1 = new Fighter(mookTemplate[1]);
		
		//Fight
		Fighter[] activeFighter = {players[0],mook1};
		Encounter encounter = new Encounter(activeFighter);
		encounter.start();
		
		System.out.println("End of program");
	}
	
	
	public static Armor[] loadArmor(String filename) {
		File file = new File(filename);
		Armor[] armors = null;
		try {
			int lineCount = countLines(file);
			String[][] armorData = new String[lineCount][13];
			Scanner sc = new Scanner(file);
			int i = 0;
			while(sc.hasNextLine()) {
				armorData[i] = sc.nextLine().split(",");
				i++;
			}
			sc.close();
			armors = new Armor[armorData.length];
			for(i = 0; i < armorData.length; i++) {
				int armorID = Integer.parseInt(armorData[i][0]);
				String name = armorData[i][1];
				int SP = Integer.parseInt(armorData[i][2]);
				armors[i] = new Armor(armorID, name, SP);
			}
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return armors;
	}
	public static MeleeWeapon[] loadMeleeWeapon(String filename) {
		File file = new File(filename);
		MeleeWeapon[] weapons = null;
		try {
			int lineCount = countLines(file);
			String[][] weaponData = new String[lineCount][5];
			Scanner sc = new Scanner(file);
			int i = 0;
			while(sc.hasNextLine()) {
				weaponData[i] = sc.nextLine().split(",");
				i++;
			}
			sc.close();
			weapons = new MeleeWeapon[weaponData.length];
			for (i = 0; i < weaponData.length; i++) {
				int weaponID = Integer.parseInt(weaponData[i][0]);
				String name = weaponData[i][1];
				int damageDiceNumber = Integer.parseInt(weaponData[i][2]);
				int damageDiceValue = Integer.parseInt(weaponData[i][3]);
				int rateOfFire = Integer.parseInt(weaponData[i][4]);
				String combatStat = weaponData[i][5];
				weapons[i] = new MeleeWeapon(weaponID, name, damageDiceNumber, damageDiceValue, rateOfFire,combatStat);
			}
			sc.close();
	
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return weapons;
	}
	public static RangedWeapon[] loadRangedWeapon(String filename) {
		File file = new File(filename);
		RangedWeapon[] weapons = null;
		try {
			int lineCount = countLines(file);
			String[][] weaponData = new String[lineCount][13];
			Scanner sc = new Scanner(file);
			int i = 0;
			while(sc.hasNextLine()) {
				weaponData[i] = sc.nextLine().split(",");
				i++;
			}
			sc.close();
			weapons = new RangedWeapon[weaponData.length];
			for(i = 0; i < weaponData.length; i++) {
				int weaponID = Integer.parseInt(weaponData[i][0]);
				String name = weaponData[i][1];
				int damageDiceNumber = Integer.parseInt(weaponData[i][2]);
				int damageDiceValue = Integer.parseInt(weaponData[i][3]);
				int rateOfFire = Integer.parseInt(weaponData[i][4]);
				int clipSize = Integer.parseInt(weaponData[i][5]);
				int clipAmmo = Integer.parseInt(weaponData[i][6]);
				String combatStat = weaponData[i][7];
				weapons[i] = new RangedWeapon(weaponID, name, damageDiceNumber, damageDiceValue, rateOfFire,clipSize,clipAmmo,combatStat);
			}
	
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return weapons;
	}
	public static Character[] loadCharacter(String filename,Armor[] armors, MeleeWeapon[] meleeWeaps, RangedWeapon[] rangedWeaps) {
		File file = new File(filename);
		Character[] fighters = null;
		try {
			int lineCount = countLines(file);
			String[][] fighterData = new String[lineCount][13];
			Scanner sc = new Scanner(file);
			int i = 0;
			while(sc.hasNextLine()) {
				fighterData[i] = sc.nextLine().split(",");
				i++;
			}
			sc.close();
			fighters = new Character[fighterData.length];
			for(i = 0; i < fighterData.length; i++) {
				String name = fighterData[i][0];
				int inte = Integer.parseInt(fighterData[i][1]);
				int ref = Integer.parseInt(fighterData[i][2]);
				int dex = Integer.parseInt(fighterData[i][3]);
				int tech = Integer.parseInt(fighterData[i][4]);
				int cool = Integer.parseInt(fighterData[i][5]);
				int will = Integer.parseInt(fighterData[i][6]);
				int move = Integer.parseInt(fighterData[i][7]);
				int body = Integer.parseInt(fighterData[i][8]);
				int emp = Integer.parseInt(fighterData[i][9]);
				int hp = Integer.parseInt(fighterData[i][10]);
				
				int handgun = Integer.parseInt(fighterData[i][15]);
				int shoulder = Integer.parseInt(fighterData[i][16]);
				int archery = Integer.parseInt(fighterData[i][17]);
				int heavy = Integer.parseInt(fighterData[i][18]);
				int meleeWeap = Integer.parseInt(fighterData[i][19]);
				int brawling = Integer.parseInt(fighterData[i][20]);
				int evasion = Integer.parseInt(fighterData[i][21]);
				
				String chestArmorName = fighterData[i][11];
				String headArmorName = fighterData[i][12];
				String weaponName = fighterData[i][13];
				boolean weaponIsMelee = Boolean.parseBoolean(fighterData[i][14]);
				
				Armor chestArmor = armors[getIndexOfArmorByName(armors, chestArmorName)];
				Armor headArmor = armors[getIndexOfArmorByName(armors, headArmorName)];
				Weapon weapon;
				
				if(weaponIsMelee) {
					weapon = meleeWeaps[getIndexOfWeaponByName(meleeWeaps, weaponName)];
				} else {
					weapon = rangedWeaps[getIndexOfWeaponByName(rangedWeaps, weaponName)];
				}
				
				fighters[i] = new Character(name, inte, ref, dex, tech, cool, will, 
						move, body, emp,
						handgun,shoulder,archery,heavy,meleeWeap,brawling,evasion, 
						hp, headArmor, chestArmor, weapon);
			}
			
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return fighters;
	}
	public static Fighter[] convertToFighter(Character[] chars) {
		Fighter[] fighters = new Fighter[chars.length];
		for(int i = 0; i< chars.length;i++) {
			fighters[i] = new Fighter(chars[i]);
		}
		return fighters;
	}
	//ChatGPT strikes
	public static int countLines(File file) {
	        int lineCount = 0;
	        BufferedReader br = null;

	        try {
	            br = new BufferedReader(new FileReader(file));
	            while (br.readLine() != null) {
	                lineCount++;
	            }
	        } catch (IOException e) {
	            e.printStackTrace();
	        } finally {
	            if (br != null) {
	                try {
	                    br.close();
	                } catch (IOException e) {
	                    e.printStackTrace();
	                }
	            }
	        }

	        return lineCount;
	    }
	

}
