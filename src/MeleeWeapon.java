
public class MeleeWeapon extends Weapon{
	public MeleeWeapon( int weaponID, String name, int damageDiceNumber, int damageDiceValue, int rateOfFire,String combatStat) {
		super(true,weaponID,name, damageDiceNumber, damageDiceValue, rateOfFire,combatStat);
		
	}
	
	public void attackWithWeapon(Fighter enemy) {
		int damage = this.getDamageRoll();
		enemy.takeDamageBody(damage, true);
	}
}
